/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

import java.util.Scanner;

/**
 *
 * @author aluno.redes
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
/*        FactoryCar genericFactory = new ConcreteFactoryCar();//tem o tipo básico assim porque o tipo da instância porderia ser de alguma fábrica específica, exemplo: Factorycar"fábrica"  concreteFactoryCar"toyota"
        
        Car car1 = genericFactory.makeCar(CarModels.gol);
        Car car2 = genericFactory.makeCar(CarModels.uno);
        Car car3 = genericFactory.makeCar(CarModels.corolla);
        
        car1.showInfo();
        car2.showInfo();
        car3.showInfo();
        
        FactoryCar toyota = new ToyotaFactory();
        
        Car car4 = toyota.makeCar(CarModels.corolla);
        Car car5 = toyota.makeCar(CarModels.gol);
        
        car4.showInfo();
        //car5.showInfo(); //deste jeito pode dar erro, por isso pode-se usar o if else para evitar

        if (car5 != null) {
            car5.showInfo();
        } else {
            System.out.println("can´t create Car");
        }
        */

        //Exercise
        System.out.println("/////////////////////////////////////////////////////\n");
        
        System.out.println("choose the factory: volvo, toyoa, mercedes");
        String chosed = sc.next().toLowerCase();
        
        FactoryCar chosedFactory = null;
        
        switch (chosed) {
            case "toyota":
                chosedFactory = new ToyotaFactory();
                break;
            case "volvo":
                chosedFactory = new VolvoFactory();
                break;
           case "mercedes":
                chosedFactory = new MercedesFactory();
                break;
            default:
                System.out.println("invalid factory option");
        }

        System.out.println("write the number of the car you want (it has to be from the factory you chosed): ");
        System.out.println(CarModels.uno.ordinal()+"-uno (Toyota)");
        System.out.println(CarModels.gol.ordinal()+"-gol (Toyota)");
        System.out.println(CarModels.corolla.ordinal()+"-corolla (Toyota)\n");
        
        System.out.println(CarModels.XC40.ordinal()+"-XC40 (Volvo)");
        System.out.println(CarModels.XC60.ordinal()+"-XC60 (Volvo)");
        System.out.println(CarModels.XC90.ordinal()+"-XC90 (Volvo)\n");
        
        System.out.println(CarModels.classe_A.ordinal()+"-classe A (mercedes)");
        System.out.println(CarModels.classe_C.ordinal()+"-classe C (mercedes)");
        System.out.println(CarModels.classe_E.ordinal()+"-classe E (mercedes)");
        int op = sc.nextInt();
                     
        Car chosedCar = chosedFactory.makeCar(CarModels.values()[op]);
        
        chosedCar.showInfo();
       
        
       /* switch (op) {
            case 3:
                return new ;
            default:
                return null;
        }*/
       // VolvoFactory VolvoFactory = new 
        
        /*Utilizando o exemplo dado em sala, implemente pelo menos 3 Fábricas específicas 
        de carros com pelo menos 2 modelos por fábrica. O Usuário pode escolher qual fábrica vai querer e quais carros dessa fábrica vai instanciar.*/
    }
    
}
