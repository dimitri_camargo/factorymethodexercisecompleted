/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author Dimitri
 */
public class MercedesFactory implements FactoryCar {

    @Override
    public Car makeCar(CarModels model) {
        switch (model) {
            case classe_A:
                return new Classe_A();
            case classe_C:
                return new Classe_C();
            case classe_E:
                return new Classe_E();
            default:
                return null;
        }
    }

}
