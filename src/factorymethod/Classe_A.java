/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author Dimitri
 */
public class Classe_A implements Car{
    @Override
    public void showInfo() {
        System.out.println("Factory: Mercedes model Classe A");
    }
    
}
